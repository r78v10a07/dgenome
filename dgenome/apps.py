from django.apps import AppConfig


class DgenomeConfig(AppConfig):
    name = 'dgenome'
